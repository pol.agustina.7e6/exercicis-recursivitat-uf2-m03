import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 14/12/2022
* TITLE: Nombre de dígits
*/

fun main (){
    val escaner = Scanner(System.`in`)
    println(nombreDigits(escaner.nextInt()))
}

fun nombreDigits(n:Int):Int{
    return if (n>=0) 1 + nombreDigits(n/10)
    else 0
}