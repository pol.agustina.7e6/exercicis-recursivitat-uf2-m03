import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 14/12/2022
* TITLE: Primers perfectes
*/

fun main (){
    val escaner = Scanner(System.`in`)
    println(primersPerfectes(escaner.nextInt()))
}

fun primer(n:Int):Boolean{
    var zeros = 0
    for (i in 1 .. n){
        if(n%i==0) zeros ++
    }
    return zeros==2
}

fun primersPerfectes(n:Int):Boolean{
    return if(sumaDigits(n)>0) primer(sumaDigits(n)) && primer(reduccioDigits(sumaDigits(n)))
    else primer(sumaDigits(n))
}

