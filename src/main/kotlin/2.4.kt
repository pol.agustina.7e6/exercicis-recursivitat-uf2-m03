import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 14/12/2022
* TITLE: Nombres creixents
*/

fun main (){
    val escaner = Scanner(System.`in`)
    println(nombresCreixents(escaner.nextInt()))
}

fun nombresCreixents(n:Int): Boolean {
    return if (n/10>0) n%10 > (n/10)%10 && nombresCreixents(n/10)
    else true
}