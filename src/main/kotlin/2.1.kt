import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 14/12/2022
* TITLE: Factorial
*/

fun main (){
    val escaner = Scanner(System.`in`)
    println(factorial(escaner.nextInt()))
}

fun factorial(n:Int):Long{
    return if (n>1) n*factorial(n-1)
    else 1
}