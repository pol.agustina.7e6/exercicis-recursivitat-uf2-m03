import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 14/12/2022
* TITLE: Reducció de dígits
*/

fun main (){
    val escaner = Scanner(System.in)
    println(reduccioDigits(escaner.nextInt()))
}

fun reduccioDigits(n:Int):Int{
    return if(n/10 > 0) reduccioDigits(sumaDigits(n))
    else n
}

fun sumaDigits(n:Int):Int{
    return if(n/10 > 0) n%10 + sumaDigits(n/10)
    else n
}