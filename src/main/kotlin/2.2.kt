import java.util.Scanner

/*
* AUTHOR: Pol Agustina Prats
* DATE: 14/12/2022
* TITLE: Doble factorial
*/

fun main (){
    val escaner = Scanner(System.`in`)
    println(dobleFactorial(escaner.nextInt()))
}

fun dobleFactorial(n:Int):Long{
    return if (n>1) n*dobleFactorial(n-2)
    else 1
}